core = 7.x
api = 2

; uw_webform_webhook
projects[uw_webform_webhook][type] = "module"
projects[uw_webform_webhook][download][type] = "git"
projects[uw_webform_webhook][download][url] = "https://git.uwaterloo.ca/mur-dev/uw_webform_webhook.git"
projects[uw_webform_webhook][download][tag] = "7.x-1.1"
projects[uw_webform_webhook][subdir] = ""
